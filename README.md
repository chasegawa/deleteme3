[[ Maketools Overview ]](./maketools/)
***

# SMS MicroService Image

This is the backing SMS microservice implementation abstracted and configurable.

This service requires connection details for the Twilio account in order to send sms. 

This image assumes that the code from this project has already built the Spring Boot Service jar.

## Base Docker Image

* [base-java-8](https://bitbucket.org/cccnext/docker-base-java/src/master/8/)

## Image Hierarchy

    |-- dockerhub/ubuntu 
        |-- base-image
            |-- base-java-8
                |-- base-newrelic
                    |--> service-sms

## Tags

tag   | notes
------|-----------------
1.0.0 | Initial release

## Ports

* 8080

## Environment Variables

Note that OPS will not allow Docker images to contain sensitive data such as passwords, therefore some of these variables will be removed in the near future.

key         | default value | description
------------|---------------|--------------
FROM_NUMBER | 16235551212   | Phone number of the sender
TWILIO_SID  |               | Twilio account Id
TWILIO_AUTH |               | Twilio auth code

## Usage

See [Maketools Overview](./maketools/) for additional usage details

### Build image
```shell
$ docker build -t <registry-hostname>/service-sms:<tag> .
```
or
```shell
$ make build
```

### Run container
```shell
$ docker run -p 8080:8080 -e FROM_NUMBER=14805551212 -e TWILIO_SID=CHANGE_ME -e TWILIO_AUTH=CHANGE_ME <registry-hostname>/service-sms:<tag>
```
or
```shell
$ make run OPT="-p 8080:8080 -e FROM_NUMBER=14805551212 -e TWILIO_SID=CHANGE_ME -e TWILIO_AUTH=CHANGE_ME"
```

### Sending messages


The *FROM_NUMBER* number requires a 11 digit number so it is necessary to include a leading 1. e.g. 14805551212.
Once the container is running use a preferred REST client such as Postman, Chrome Advance REST Client Application, curl, etc

Service is available at

* http://[server]:8080/ccc/api/sms/v1/sendSMS

#### Sample JSON
```json
{
    "phoneNumber":"4805551212",
    "subject":"SMS subject",
    "body":"The body of the text message"
}
```
***
[[ Maketools Overview ]](./maketools/)
