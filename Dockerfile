# SMS Microservice Image
#
# NAMESPACE: docker.dev.ccctechcenter.org
# IMAGE NAME: service-sms

FROM docker.dev.ccctechcenter.org/base-newrelic:1.1.0
MAINTAINER Unicon, Inc.

#OPS will not allow credentials to be stored in Dockerfiles. Alternative method pending.
ENV FROM_NUMBER=16235551212 \
    TWILIO_SID=CHANGE_ME \
    TWILIO_AUTH=CHANGE_ME

RUN  ln -sf /dev/stdout /var/log/service-sms.log \
  && ln -sf /dev/stderr /var/log/service-sms.error.log

EXPOSE 8080

COPY includes/ /

CMD ["/bin/bash", "/runService.sh"]
