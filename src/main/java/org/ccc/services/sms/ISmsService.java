package org.ccc.services.sms;

import java.util.List;

import org.ccc.services.sms.service.ServiceException;

/**
 * @author chasegawa@unicon.net
 */
public interface ISmsService {
    /**
     * Signature for sending an SMS message. 
     * @param phoneNumber - target of the SMS message
     * @param subject - subject of the SMS message
     * @param body - body text of the SMS message
     * @throws ServiceException - generalized RTE to wrap implementation specific exception types.
     */
    void sendSms(String phoneNumber, String subject, String body) throws ServiceException;
    
    /**
     * Signature for sending multiple SMS messages. 
     * @param phoneNumbers - list of target numbers 
     * @param subject - subject of the SMS message
     * @param body - body text of the SMS message
     * @throws ServiceException - generalized RTE to wrap implementation specific exception types.
     */
    void sendBulkSms(List<String> phoneNumbers, String subject, String body) throws ServiceException;
}
