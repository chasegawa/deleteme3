package org.ccc.services.sms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.ccc.services.sms.ISmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;

/**
 * Service implementation for the SmsService
 * 
 * @author chasegawa@unicon.net
 */
@Service
public class TwilioBasedSmsService implements ISmsService {
    private static final Logger LOG = LoggerFactory.getLogger(TwilioBasedSmsService.class);

    @Value("${twilio.account.sid}")
    private String accountSid;

    @Value("${twilio.auth.token}")
    private String authToken;

    @Value("${ccc.sms.format:Subject: <subject> --- Message: <body>}") // use <subject> and <body> placeholders
    private String smsFormat;

    @Value("${ccc.sms.from.phone}")
    private String smsFromPhone;

    private Account twilioAccount;

    private TwilioRestClient twilioRestClient;

    private void attemptTwilioInit() {
        if (!StringUtils.isEmpty(this.accountSid) && !StringUtils.isEmpty(this.authToken)) {
            this.twilioRestClient = new TwilioRestClient(this.accountSid, this.authToken);
            this.twilioAccount = this.twilioRestClient.getAccount();
        }
    }

    private List<NameValuePair> createMessageParameters(final String phoneNumber, final String subject, String body)
                    throws ServiceException {
        final String messageBody = this.smsFormat.replace("<subject>", subject).replace("<body>", body);
        final List<NameValuePair> messageParams = new ArrayList<NameValuePair>();
        messageParams.add(new BasicNameValuePair("To", phoneNumber));
        messageParams.add(new BasicNameValuePair("From", this.smsFromPhone));
        messageParams.add(new BasicNameValuePair("Body", messageBody));
        return messageParams;
    }

    private boolean isTwilioActive() {
        if (this.twilioRestClient == null) {
            this.attemptTwilioInit();
        }
        return this.twilioRestClient != null;
    }

    @Override
    public void sendBulkSms(List<String> phoneNumbers, String subject, String body) throws ServiceException {
        for (String phoneNumber : phoneNumbers) {
            sendSms(phoneNumber, subject, body);
        }
    }

    private void sendSms(final List<NameValuePair> messageParams) throws ServiceException {
        // Send an SMS (Requires version 3.4+)
        try {
            final MessageFactory messageFactory = this.twilioAccount.getMessageFactory();
            messageFactory.create(messageParams);
        }
        catch (TwilioRestException e) {
            final String exceptionMessage = "Twilio SMS send did not succeed: [code: %s], [message: %s]";
            throw new ServiceException(String.format(exceptionMessage, e.getErrorCode(), e.getErrorMessage()));
        }
    }

    @Override
    public void sendSms(String phoneNumber, String subject, String body) throws ServiceException {
        if (this.isTwilioActive()) {
            final List<NameValuePair> messageParams = this.createMessageParameters(phoneNumber, subject, body);
            this.sendSms(messageParams);
        } else {
            LOG.warn("Twilio is not active.  No account SID and/or auth token has been configured.");
        }
    }
}
