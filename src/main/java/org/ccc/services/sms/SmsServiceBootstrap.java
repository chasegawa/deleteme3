package org.ccc.services.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entrypoint / bootstrap point for the SMS service
 * @author chasegawa@unicon.net
 */
@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class SmsServiceBootstrap {
    public static void main(String[] args) {
        new SpringApplication(SmsServiceBootstrap.class).run(args);
    }
}
