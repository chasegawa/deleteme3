package org.ccc.services.sms.controller;

import java.util.List;
import java.util.Map;

import org.ccc.services.sms.ISmsService;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.annotation.ApiVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for sending SMS messages.
 * @author chasegawa@unicon.net
 */
@Api(name = "SMS service", description = "Methods for sending SMS messages", group = "Communications")
@ApiVersion(since = "1.0")
@RestController
@RequestMapping(value = SmsRestController.REQUEST_ROOT, produces = MediaType.APPLICATION_JSON_VALUE)
public class SmsRestController {
    static final String REQUEST_ROOT = "/ccc/api/sms/v1";

    @Autowired
    private ISmsService service;

    @ApiMethod(summary = "Service method to send SMS messages to a specified list of numbers",
               description = "Sends an SMS text message to specified list of numbers"
                           + "Reads a map of values from the request body of the needed values for the specific event being created. Valid values:"
                           + "<li>phoneNumbers - the list of target numbers of the SMS message"
                           + "<li>subject - the subject of the message"
                           + "<li>body - the text message body")
    @RequestMapping(method = RequestMethod.POST, value = "/sendBulkSMS", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @SuppressWarnings("unchecked")
    public @ApiResponseObject void sendBulkSms(@ApiBodyObject @RequestBody Map<String, Object> args) {
        service.sendBulkSms((List<String>) args.get("phoneNumbers"), args.get("subject").toString(), args.get("body").toString());
    }
    
    @ApiMethod(summary = "Service method to send SMS messages to a specified number",
                    description = "Sends an SMS text message to specified number"
                                + "Reads a map of values from the request body of the needed values for the specific event being created. Valid values:"
                                + "<li>phoneNumber - the target of the SMS message"
                                + "<li>subject - the subject of the message"
                                + "<li>body - the text message body")
     @RequestMapping(method = RequestMethod.POST, value = "/sendSMS", consumes = MediaType.APPLICATION_JSON_VALUE)
     @ResponseStatus(HttpStatus.CREATED)
     public @ApiResponseObject void sendSms(@ApiBodyObject @RequestBody Map<String, String> args) {
         service.sendSms(args.get("phoneNumber"), args.get("subject"), args.get("body"));
     }
}
