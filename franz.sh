#!/bin/bash

git config --global push.default simple
git fetch
git checkout origin develop
git pull origin develop

cat pom.xml > foo.xml

git add foo.xml
git commit -m "NOJIRA Jenkins updating metadata for pilot/prod"
git push origin HEAD:develop