#!/bin/bash

# Launch the service
COM_STRING="-Dspring.config.location=classpath:application.properties,/opt/ccc/config/creds.properties -jar /opt/ccc/sms-service.jar"

#### if no creds.properties file, then add the ENV vars
if [ ! -f /opt/ccc/config/creds.properties ]; then
  COM_STRING="$COM_STRING --ccc.sms.from.phone=$FROM_NUMBER --twilio.account.sid=$TWILIO_SID --twilio.auth.token=$TWILIO_AUTH"
fi

# Checks whether to enable New Relic APM
. /opt/newrelic/bin/set_newrelic.sh

COM_STRING="$JAVA_OPTS $COM_STRING"

echo "Using $COM_STRING"

# Launch the service
java $COM_STRING
